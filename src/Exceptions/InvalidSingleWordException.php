<?php

namespace JochenTimmermans\Syllables\Exceptions;

use Exception;

/**
 * Class InvalidSingleWordException
 */
class InvalidSingleWordException extends Exception
{
    /** @var string $message */
    protected $message = 'INVALID_SINGLE_WORD';
}