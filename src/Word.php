<?php

namespace JochenTimmermans\Syllables;

use JochenTimmermans\Syllables\Exceptions\InvalidSingleWordException;

/**
 * Class Word
 */
class Word
{
    /** @var string $word */
    private string $word;

    /**
     * @param string $word
     * @throws InvalidSingleWordException
     */
    public function __construct(string $word)
    {
        if (str_contains($word, ' ')) {
            throw new InvalidSingleWordException();
        }

        $this->word = $word;
    }

    /**
     * Counts the vowels in a word.
     *
     * @param bool $countTrailingE
     * @return int
     */
    public function vowelCount(bool $countTrailingE = false): int
    {
        $vowels = ['a', 'e', 'i', 'o', 'u'];
        $charArray = str_split(strtolower($this->word));

        $vowelsInWord = array_filter($charArray, fn($char) => in_array($char, $vowels));

        // Strip trailing e's - only if it's not the only vowel (e.g. the)
        if ($countTrailingE === false && end($charArray) === 'e' && count($vowelsInWord) > 1) {
            array_pop($vowelsInWord);
        }

        return count($vowelsInWord);
    }

    /**
     * Count the amount of syllables in a word
     *
     * @return int
     */
    public function syllableCount(): int
    {
        return $this->vowelCount();
    }
}