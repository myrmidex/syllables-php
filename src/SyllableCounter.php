<?php

namespace JochenTimmermans\Syllables;

use Exception;
use JochenTimmermans\Syllables\Exceptions\InvalidSingleWordException;

/**
 * Class Syllables
 */
class SyllableCounter
{
    /**
     * Count the number of syllables in a word or text
     *
     * @param string $text
     * @return int
     * @throws Exception
     */
    public static function count(string $text): int
    {
        return array_sum(
            array_map(
                fn($word) => self::countInWord($word),
                explode(' ', $text)
            )
        );
    }

    /**
     * Count the number of syllables in a single word.
     *
     * @param string $wordStr
     * @return int
     * @throws Exception
     */
    public static function countInWord(string $wordStr): int
    {
        if (str_contains($wordStr, ' ')) {
            throw new InvalidSingleWordException();
        }

        $word = new Word($wordStr);

        return $word->vowelCount();
    }
}