<?php

namespace JochenTimmermans\Syllables\Test;

use Exception;
use JochenTimmermans\Syllables\Exceptions\InvalidSingleWordException;
use JochenTimmermans\Syllables\SyllableCounter;
use JochenTimmermans\Syllables\Word;
use PHPUnit\Framework\TestCase;

/**
 * Class WordTest
 *
 * @covers \JochenTimmermans\Syllables\Word
 */
class WordTest extends TestCase
{
    /**
     * Test that the constructor does not allow for spaces
     *
     * @return void
     * @covers \JochenTimmermans\Syllables\Word::__construct
     * @throws Exception
     */
    public function test_it_throws_an_exception_if_not_a_single_word(): void
    {
        $this->expectExceptionMessage('INVALID_SINGLE_WORD');

        new Word('a bread');
    }

    /**
     * Test that it judges syllable count by the amount of vowels.
     *
     * @return void
     * @covers \JochenTimmermans\Syllables\Word::vowelCount
     * @throws Exception
     */
    public function test_it_counts_vowels_properly(): void
    {
        $words = [
            'a' => 1,
            'the' => 1,
            'plant' => 1,
            'banana' => 3,
            'children' => 2,
            'camera' => 3,
        ];

        foreach ($words as $word => $expectedSyllableCount) {
            $word = new Word($word);
            $this->assertEquals($expectedSyllableCount, $word->vowelCount());
        }
    }

    /**
     * Test that trailing e's are not taken into account when counting vowels
     *
     * @return void
     * @throws Exception
     */
    public function test_it_counts_vowels_but_ignores_trailing_e(): void
    {
        $words = [
            'tape' => 1,
            'like' => 1,
            'love' => 1,
            'extreme' => 2,
            'take' => 1,
            'blue' => 1,
        ];

        foreach ($words as $wordStr => $expectedSyllableCount) {
            $word = new Word($wordStr);
            $this->assertEquals($expectedSyllableCount, $word->vowelCount());
        }
    }

    /**
     * @param string $wordStr
     * @param int $expectedCount
     * @return void
     * @throws InvalidSingleWordException
     * @covers       \JochenTimmermans\Syllables\Word::syllableCount
     * @dataProvider data_it_counts_the_syllables_correctly_in_a_single_word
     */
    public function test_it_counts_the_syllables_correctly_in_a_single_word(string $wordStr, int $expectedCount): void
    {
        $word = new Word($wordStr);
        $this->assertEquals($expectedCount, $word->syllableCount());
    }


    /**
     * Data for syllable testing
     *
     * @return array
     */
    public static function data_it_counts_the_syllables_correctly_in_a_single_word(): array
    {
        return [
            [
                'text' => 'a',
                'expectedCount' => 1,
            ],
//            [
//                'text' => 'bread',
//                'expectedCount' => 1,
//            ],
//            [
//                'text' => 'female',
//                'expectedCount' => 2,
//            ],
//            [
//                'text' => 'bicycle',
//                'expectedCount' => 3,
//            ],
            [
                'text' => 'interesting',
                'expectedCount' => 4,
            ],
        ];
    }
}