<?php

namespace JochenTimmermans\Syllables\Test;

use Exception;
use JochenTimmermans\Syllables\SyllableCounter;
use PHPUnit\Framework\TestCase;

/**
 * Class Syllables
 *
 * @covers \JochenTimmermans\Syllables\SyllableCounter
 */
class SyllableCounterTest extends TestCase
{
    /**
     * @return void
     * @covers \JochenTimmermans\Syllables\SyllableCounter::count
     * @throws Exception
     */
    public function test_it_counts_the_syllables_correctly_in_a_text(): void
    {
        $this->assertEquals(2, SyllableCounter::count('a bread'));
    }

    /**
     * @return void
     * @covers \JochenTimmermans\Syllables\SyllableCounter::countInWord
     * @throws Exception
     */
    public function test_it_throws_an_exception_if_not_a_single_word(): void
    {
        $this->expectExceptionMessage('INVALID_SINGLE_WORD');

        SyllableCounter::countInWord('a bread');
    }


    /**
     * Test that it judges syllable count by the amount of vowels.
     *
     * @return void
     * @covers \JochenTimmermans\Syllables\SyllableCounter::countInWord
     * @throws Exception
     */
    public function test_it_counts_syllables_by_the_amount_of_vowels(): void
    {
        $words = [
            'a' => 1,
            'the' => 1,
            'plant' => 1,
            'banana' => 3,
            'children' => 2,
            'camera' => 3,
        ];

        foreach ($words as $word => $expectedSyllableCount) {
            $this->assertEquals($expectedSyllableCount, SyllableCounter::countInWord($word));
        }
    }

    /**
     * Test that trailing e's are not taken into account when counting vowels
     *
     * @return void
     * @throws Exception
     */
    public function _test_it_counts_syllables_by_the_amount_of_vowels_but_ignores_trailing_e(): void
    {
        $words = [
            'tape' => 1,
            'like' => 1,
            'love' => 1,
            'extreme' => 2,
            'take' => 1,
            'blue' => 1,
        ];

        foreach ($words as $word => $expectedSyllableCount) {
            $this->assertEquals($expectedSyllableCount, SyllableCounter::countInWord($word));
        }
    }
}